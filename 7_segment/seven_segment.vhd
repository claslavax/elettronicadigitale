LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY seven_segment IS
     PORT(SW   :IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	       HEXO :OUT STD_LOGIC_VECTOR(0 TO 6));
END seven_segment;

ARCHITECTURE Behavior OF seven_segment IS

BEGIN
HEXO(0)<=(SW(0)) NAND(SW(2));
HEXO(1)<=NOT (SW(1) XOR SW(0)) NAND(SW(2));
HEXO(2)<=NOT (SW(1) XOR SW(0)) NAND(SW(2));
HEXO(3)<=NOT (SW(1) NOR SW(0)) NAND(SW(2));
HEXO(4)<=NOT (SW(2));
HEXO(5)<=NOT (SW(2));
HEXO(6)<=NOT (SW(1)) NAND(SW(2));
--clara � bella
--felrnvewrmjvp�o
END Behavior;